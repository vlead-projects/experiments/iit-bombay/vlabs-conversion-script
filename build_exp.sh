#!/bin/bash

echo "number of arguments =  $#"
if [ "$#" -ne 2 ]; then
    echo "Usage: ./script.sh <url> <location>"
    exit 1
fi

url=$1
location=$2

cd /home/$USER/Documents/

##### Clone repository
git clone $url

basename=$(basename $url)
filename=${basename%.*}
expname=$filename

#### Rename the repository to experiment name
mv $filename round-template

##### Clone vlabs template
cd /home/$USER/        ######### Moving into home folder

if [ ! -d "vlabs-conversion-template" ]
then
    git clone http://vlabs.iitb.ac.in/gitlab/jai/vlabs-conversion-template.git
fi

cp -r vlabs-conversion-template /home/$USER/Documents/
template=vlabs-conversion-template


#### Conversion process starts
echo $USER

echo $expname
echo $template
cd /home/$USER/Documents/

mv $template $expname

cp -r round-template $expname

cd $expname/round-template/experiment/simulation/
cp -r images ../../../
cd ..
cp -r images/* ../../images/



cd $location
if [ -d "$expname" ]; then rm -Rf $expname; fi
cd /home/$USER/Documents/
cp -r $expname $location

cd /home/$USER/Documents/
rm -rf $expname
rm -rf round-template
exit 0
